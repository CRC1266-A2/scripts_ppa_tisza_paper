# Scripts_PPA_Tisza_Paper

The analysis folder of this repository contains the scripts required to reproduce the analysis conducted in the context of the Paper "A methodological framework for spatial analyses using archaeological and environmental data --- A case study from the Carpathian Basin" by Kempf and Günther (2022).

To run the '.Rmd', you need to download the repository and place the required datasets in the corresponding sub-folder of the data folder. 

## Data availability

- The digital elevation data used for this analysis can be accessed (after a registration) using this link: 'https://download.geoservice.dlr.de/TDM90/' (last accessed, 1st of August 2022). 

- The soil data used for this analysis can be accessed (using the request form) using this link: 'https://esdac.jrc.ec.europa.eu/content/european-soil-database-v20-vector-and-attribute-data' (last accessed, 1st of August 2022). 

- The archaeological sites used for the analysis cannot be made publicly available. Thus, we generated random points in the study area, which can be accessed using this link: 'https://cloud.rz.uni-kiel.de/index.php/s/9Jrma7EYCwqjRW5' (last accessed, 1st of August 2022) (We use the CAU-Cloud during the review because scripts tend to change. We will store the data on Zenodo as soon as the review is finished and update this section). The scripts work the same way, but the plots show different results.

- The outline of the study area can be accessed using this link: 'https://cloud.rz.uni-kiel.de/index.php/s/9Jrma7EYCwqjRW5' (last accessed, 1st of August 2022) (We use the CAU-Cloud during the review because scripts tend to change. We will store the data on Zenodo as soon as the review is finished and update this section).

## rbias

- The latest version of the currently unpublished 'rbias' package can be accessed using this link: 'https://cloud.rz.uni-kiel.de/index.php/s/9Jrma7EYCwqjRW5'. After the download, the package can be installed using 'devtools::install_local("path/to/rbias.zip")' (last accessed, 1st of August 2022) (We use the CAU-Cloud during the review because scripts tend to change. We will store the data on Zenodo as soon as the review is finished).

## Local folder structure

The local folder structure should look like this to run the script:

```
📦Analysis
 ┣ 📂data
 ┃ ┣ 📂derived_data
 ┃ ┃ ┣ 📜dem_smooth.tif
 ┃ ┃ ┗ 📜study_area.gpkg
 ┃ ┗ 📂raw_data
 ┃ ┃ ┣ 📂ESDB
 ┃ ┃ ┃ ┣ 📜SGDB_PTR.dbf
 ┃ ┃ ┃ ┣ 📜SGDB_PTR.prj
 ┃ ┃ ┃ ┣ 📜SGDB_PTR.sbn
 ┃ ┃ ┃ ┣ 📜SGDB_PTR.sbx
 ┃ ┃ ┃ ┣ 📜SGDB_PTR.shp
 ┃ ┃ ┃ ┣ 📜SGDB_PTR.shp.xml
 ┃ ┃ ┃ ┗ 📜SGDB_PTR.shx
 ┃ ┃ ┗ 📜sites.gpkg
 ┗ 📜Analysis.Rmd
 ``` 
